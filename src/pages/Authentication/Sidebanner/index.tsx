import React from 'react';
import classNames from 'classnames/bind';
import styles from '../css/Authentication.module.css';
import Logo from '../img/logo.svg';

type BannerProp = {
  bannerImg: string;
};

const Sidebanner = ({ bannerImg }: BannerProp) => {
  const cx = classNames.bind(styles);

  return (
    <>
      <a className={`${cx({ navbarBrand: true })}`} href="/">
        <img src={Logo} className={`${cx({ logo: true })} `} alt="logo" />
      </a>
      <div className="container">
        <div className={`${cx({ imgContainer: true })} my-auto text-center`}>
          <img src={bannerImg} className="img-fluid w-100" alt="..." />
        </div>
      </div>
    </>
  );
};

export default Sidebanner;
