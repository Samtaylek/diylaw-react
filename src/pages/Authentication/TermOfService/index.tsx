import React from 'react';
import classNames from 'classnames/bind';
import styles from '../css/Authentication.module.css';
import Sidebanner from '../Sidebanner';
import HeroImg from '../img/hero5.svg';
import Terms from '../../../utils';

const TermOfSerice = () => {
  const cx = classNames.bind(styles);
  return (
    <>
      <main className={styles.viewHeight}>
        <div className="row">
          <div
            className={`${cx({ maskBg: true })} col-md-5 align-items-center d-sm-flex d-none p-4`}
          >
            <Sidebanner bannerImg={HeroImg} />
          </div>
          <div className="col-md-7 align-items-center d-flex">
            <div className="container p-4">
              <div className="row">
                <div className="col-md-10 mx-auto">
                  <h2 className={`${cx({ header: true })} mb-5`}>Term of Service</h2>
                  <ul className={`${cx({ termsList: true })} text-grey`}>
                    {Terms.map((term) => (
                      <li key={term.key}>{term.content}</li>
                    ))}
                  </ul>
                  <div className="row">
                    <div className="col-lg-5 col-sm-12  mt-3 d-grid">
                      <button type="button" className="button button-primary text-uppercase">
                        Accept the terms
                      </button>
                    </div>
                    <div className="col-lg-5 col-sm-12  mt-3 d-grid">
                      <button type="button" className="button button-outline-danger text-uppercase">
                        Decline
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </>
  );
};

export default TermOfSerice;
