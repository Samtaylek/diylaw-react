import React from 'react';
import classNames from 'classnames/bind';
import { Button } from 'antd';
import styles from '../css/Authentication.module.css';
import Sidebanner from '../Sidebanner';
import HeroImg from '../img/hero4.svg';

const SignupSuccess = () => {
  const cx = classNames.bind(styles);
  return (
    <>
      <main className={styles.viewHeight}>
        <div className="row">
          <div
            className={`${cx({ maskBg: true })} col-md-5 align-items-center d-sm-flex d-none p-4`}
          >
            <Sidebanner bannerImg={HeroImg} />
          </div>
          <div className="col-md-7 align-items-center d-flex">
            <div className="container p-4">
              <div className="row">
                <div className="col-md-8 mx-auto">
                  <h2 className={`${cx({ header: true })}`}>Check your email</h2>
                  <p className="text-grey">
                    We have sent an email to you with a link to confirm your account.
                  </p>

                  <h2 className={`${cx({ header: true })} mt-5`}>Didn&lsquo;t get the email?</h2>
                  <p>
                    If you don&rsquo;t see an email from us within 5 minutes, one of things could
                    have happened:
                  </p>
                  <ol>
                    <li className="py-2">The email is in your spam folder</li>
                    <li className="py-2">The email address you entered had a typo</li>
                    <li className="py-2">
                      you accidentally entered another email address. (Usually happens with
                      auto-complete)
                    </li>
                    <li className="py-2">
                      We can&#39;t deliver the email to this address. (usually because of corporate
                      firewalls or filtering)
                    </li>
                  </ol>

                  <Button className={`${cx({ submitBtn: true })}`}>Return to Home</Button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </>
  );
};

export default SignupSuccess;
