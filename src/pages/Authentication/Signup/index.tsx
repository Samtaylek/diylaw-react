import React from 'react';
import classNames from 'classnames/bind';
import { Button, Input, Form, Checkbox } from 'antd';
import styles from '../css/Authentication.module.css';
import Sidebanner from '../Sidebanner';
import HeroImg from '../img/hero.svg';
import GoogleBtn from '../img/google.svg';
import FacebookBtn from '../img/facebook.svg';

const Signup = () => {
  const cx = classNames.bind(styles);

  return (
    <>
      <main className={styles.viewHeight}>
        <div className="row">
          <div
            className={`${cx({ maskBg: true })} col-md-5 align-items-center d-sm-flex d-none p-4`}
          >
            <Sidebanner bannerImg={HeroImg} />
          </div>
          <div className={`${cx()} col-md-7`}>
            <div className="container p-sm-5 py-5">
              <p className="text-grey text-end">
                Already have an account?{' '}
                <a href="/login" className="text-green text-decoration-none">
                  Sign In
                </a>
              </p>
              <div className="row px-lg-5 mt-5">
                <div className="col-md-10 mx-auto">
                  <h2 className={`${cx({ header: true })}`}>Create an account</h2>
                  {/* <Spin delay={500}> */}
                  <div className="row mt-5">
                    <Form name="signup" className="signup-form" layout="vertical">
                      <Form.Item
                        name="firstName"
                        rules={[{ required: true, message: 'Please input your First Name!' }]}
                      >
                        <Input placeholder="First Name" className={`${cx({ formInput: true })}`} />
                      </Form.Item>
                      <Form.Item
                        name="lastName"
                        rules={[{ required: true, message: 'Please input your Last Name!' }]}
                      >
                        <Input placeholder="Last Name" className={`${cx({ formInput: true })}`} />
                      </Form.Item>
                      <Form.Item
                        name="email"
                        rules={[
                          { type: 'email', message: 'Please input a valid email!' },
                          { required: true, message: 'Please input your email!' },
                        ]}
                      >
                        <Input
                          placeholder="Email"
                          type="email"
                          className={`${cx({ formInput: true })}`}
                        />
                      </Form.Item>
                      <Form.Item
                        name="password"
                        rules={[{ required: true, message: 'Please input your password!' }]}
                      >
                        <Input.Password
                          placeholder="Password"
                          className={`${cx({ formInput: true })}`}
                        />
                      </Form.Item>
                      <Form.Item name="remember" valuePropName="checked">
                        <Checkbox className="">
                          I have read, understood and I agree to DIYlaw Privacy Policy, and Terms
                          and conditions.
                        </Checkbox>
                      </Form.Item>
                      <Form.Item>
                        <Button className={`${cx({ submitBtn: true })}`} htmlType="submit">
                          Register
                        </Button>
                      </Form.Item>
                    </Form>
                  </div>
                  <div className={styles.hrSect}>Or</div>
                  <div className="my-3">
                    <Button className={`${cx({ socialBtn: true })}`}>
                      <img src={GoogleBtn} className="mx-4" alt="..." /> Sign up with Google
                    </Button>
                  </div>
                  <div className="my-3">
                    <Button className={`${cx({ socialBtn: true })}`}>
                      <img src={FacebookBtn} className="mx-4" alt="..." /> Sign up with Facebook
                    </Button>
                  </div>
                  {/* </Spin> */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </>
  );
};

export default Signup;
