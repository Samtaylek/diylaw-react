const Terms: { key: string; content: string }[] = [
  {
    key: 'T1',
    content:
      'DIYlaw is unable to perform any validation. What you type is exactly what you get and will appear in your registration documents. Please pay close attention to the information you input.',
  },
  {
    key: 'T2',
    content:
      'Once we have verified your order no further changes can be made to the documents. Please ensure that you are 100% confident the information you enter is correct.',
  },
  {
    key: 'T3',
    content:
      'When completing your questionnaire, please do NOT guess or assume. If you are not sure about anything, stop your work, save and contact us for clarification.',
  },
  {
    key: 'T4',
    content:
      'When completing any questionnaire on DIYlaw, please read all instructions and warnings very carefully - they are there for your protection! DIYlaw products are legally binding, so please be extremely careful. Do not rush the process.',
  },
  {
    key: 'T5',
    content:
      'DIYlaw is NOT your lawyer, nor are its employees. No lawyer-client relationship is created by you using DIYlaw. We are a technology platform providing legal information for self-help and automated software solutions for legal processes. For legal advice or review, you should seek the assistance of a lawyer.',
  },
  {
    key: 'T6',
    content: "You agree to the DIYlaw's Terms of Service and accept DIYlaw's Privacy Policy.",
  },
];

export default Terms;
