import React, { FC, Suspense, lazy } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Spin } from 'antd';

const Signup = lazy(() => import('./pages/Authentication/Signup'));
const Login = lazy(() => import('./pages/Authentication/Login'));
const SignupSuccess = lazy(() => import('./pages/Authentication/SignupSuccess'));
const TermOfService = lazy(() => import('./pages/Authentication/TermOfService'));
/**
 * Defines Application Routes
 * @function
 */

const Routes = () => {
  interface styling {
    margin: string;
    marginBottom: string;
    height: string;
    padding: string;
    TextAlign: string;
    borderRadius: string;
  }

  const styles: styling = {
    margin: '20px 0',
    marginBottom: '20px',
    height: '100vh',
    padding: '20% 50%',
    TextAlign: 'center',
    borderRadius: '4px',
  };

  const Spinner: FC = () => (
    <div style={styles}>
      <Spin size="large" />
    </div>
  );

  return (
    <Router>
      <Suspense fallback={<Spinner />}>
        <Switch>
          <Route exact path="/" component={Signup} />
          <Route exact path="/signup" component={Signup} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/success" component={SignupSuccess} />
          <Route exact path="/term-of-service" component={TermOfService} />
        </Switch>
      </Suspense>
    </Router>
  );
};

export default Routes;
