import api from './api';
import routes from './routes';

// Authentication
interface User {
  first_name: string;
  last_name: string;
  email: string;
  password: string;
  phone: string;
}

// Register User
export const registerUser = ({ first_name, last_name, email, password, phone }: User) => {
  const url = routes.REGISTER;
  return api.post(url, { first_name, last_name, email, password, phone });
};

// Login User
export const loginUser = ({ email, password }: User) => {
  const url = routes.LOGIN;
  return api.post(url, { email, password });
};
