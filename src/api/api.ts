import axios from 'axios';
import createAuthRefreshInterceptor from 'axios-auth-refresh';
import routes from './routes';

const api = axios.create({
  baseURL: `${process.env.REACT_APP_BASE_URL}`,
  headers: {
    'Content-Type': 'application/json',
  },
});

api.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem('token');
    const settings = config;
    if (token) {
      settings.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  (error) => Promise.reject(error)
);

const refreshAuthLogic = (failedRequest: any): any => {
  api.post(routes.LOGIN).then((tokenRefreshResponse: any) => {
    localStorage.setItem('user', JSON.stringify(tokenRefreshResponse.data));
    const request = failedRequest;
    request.response.config.headers.Authorization = `Bearer ${tokenRefreshResponse.data.token}`;
    return Promise.resolve();
  });
};

createAuthRefreshInterceptor(api, refreshAuthLogic, {
  statusCodes: [401, 403],
});

export default api;
