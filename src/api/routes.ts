const routes = {
  LOGIN: 'login',
  REGISTER: 'register',
};

export default routes;
